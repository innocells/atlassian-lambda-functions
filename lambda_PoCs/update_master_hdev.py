import os, json, datetime

def lambda_handler(event, context):
    issue =json.loads(event['body'])
    parent = issue['fields']['customfield_10400']

    fields = {}
    fields['summary'] = issue['fields']['summary']
    fields['description'] = f'updated at {datetime.datetime.now().time()}'

    if parent[:4] == "BST-":
        return update_fields(server='dev-innocells.atlassian.net', 
                access_token=os.environ['JIRA_PERSONAL_ACCESS_TOKEN'],
                issueID=parent,
                fields=fields)
    else:
        response = {}
        response['errorMessages'] = []
        response['errorMessages'].append("Issue does not exist or you do not have permission to see it.")
        response['errors'] = {}
        return {
            'statusCode': 404,
            'body': json.dumps(response)
        }

def update_fields(server, access_token, issueID, fields):
    import urllib3
    http = urllib3.PoolManager()

    url = f"https://{server}/rest/api/latest/issue/{issueID}"

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": f"Basic {access_token}"
    }

    payload = {}
    payload['fields'] = fields

    response = http.request(
        method="PUT",
        url=url,
        body=json.dumps(payload),
        headers=headers,
        retries = False
    )

    if response.status==204 :
        return {
            'statusCode': response.status,
            'body': ''
        }
    else:
        return {
            'statusCode': response.status,
            'body': response.data.decode('utf-8')
        }