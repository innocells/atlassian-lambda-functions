import os, json, html
SERVER = 'uat-innocells.atlassian.net'
HTML_HEADER = "<!DOCTYPE html><html lang='es'><head><title>Export</title></head><body><table>"
HTML_THEADR = ['Id', 'Summary', 'Description', 'Status', 'Updated', 'URL']
HTML_FOOTER = "</table></body></html>"

def lambda_handler(event, context):
    response = []
    response.append(HTML_HEADER)
    response.append("<thead>"+list_to_row('th',HTML_THEADR)+"</thead>")
    response.append("<tbody>")
    jql = get_jql(
        server=SERVER, 
        access_token=os.environ['JIRA_PERSONAL_ACCESS_TOKEN'],
        jql="project=BST")

    for issue in jql['issues']:
        values = []
        values.append(issue['key'])
        values.append(issue['fields']['summary'])
        values.append(issue['fields']['description'])
        values.append(issue['fields']['status']['name'])
        values.append(issue['fields']['updated'])
        values.append(f"https://{SERVER}/browse/{issue['key']}")
        response.append(list_to_row('td',values))

    response.append("</tbody>")
    response.append(HTML_FOOTER)

    print('\n'.join(response))
    return {
        'statusCode': 200,
        'body': '\n'.join(response)
    }

def list_to_row(tag, list):
    row = []
    for item in list:
        row.append(f"<{tag}>{html.escape(item) if item is not None else ''}</{tag}>")
    return '<tr>'+''.join(row)+'</tr>'


def get_jql(server, access_token, jql):
    import urllib3
    http = urllib3.PoolManager()
    url = f"https://{server}/rest/api/latest/search?jql={jql}"

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": f"Basic {access_token}"
    }

    response = http.request(
        method="GET",
        url=url,
        headers=headers,
        retries = False
    )

    return json.loads(response.data)

# Testing locally
if __name__ == '__main__':
    lambda_handler("","")

'''
jql['issues'][0]['id']
jql['issues'][0]['fields']['summary']
jql['issues'][0]['fields']['description']
jql['issues'][0]['fields']['status']['name']
jql['issues'][0]['fields']['updated']
'''