import os, json, urllib3, tempfile
import boto3
from pptx import Presentation
from pptx.util import Inches

SERVER = 'uat-innocells.atlassian.net'
HTTP = urllib3.PoolManager()

def lambda_handler(event, context):
    issueKey = event["queryStringParameters"]["issueKey"]

    if issueKey[:8] != "BSDROAD-":
        response = {}
        response['errorMessages'] = []
        response['errorMessages'].append("Issue does not exist or you do not have permission to see it.")
        response['errors'] = {}
        return {
            'statusCode': 404,
            'body': json.dumps(response)
        }

    filename = f'{issueKey}.pptx'
    tmpfile  = f'{tempfile.gettempdir()}//{filename}'

    issue = get_issue(
        server=SERVER, 
        access_token=os.environ['JIRA_PERSONAL_ACCESS_TOKEN'],
        issueKey=issueKey)

    ppt = generate_ppt(issue)
    ppt.save(tmpfile)
    upload_to_s3(tmpfile, filename)

    return {
        'statusCode': 302,
        'body': f'https://c31790cf-3494-4967-8e39-91d8f3e612c4-innocells.s3.eu-west-1.amazonaws.com/PoC-ppt/{filename}'
    }

def get_issue(server, access_token, issueKey):
    url = f"https://{server}/rest/api/latest/issue/{issueKey}?fields=id,summary&expand="

    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": f"Basic {access_token}"
    }

    response = HTTP.request(
        method="GET",
        url=url,
        headers=headers,
        retries = False
    )

    return json.loads(response.data)

def generate_ppt(issue):
        # Creating Object
    ppt = Presentation()

    # To create blank slide layout
    # We have to use 6 as an argument
    # of slide_layouts
    blank_slide_layout = ppt.slide_layouts[6]

    # Attaching slide obj to slide
    slide = ppt.slides.add_slide(blank_slide_layout)

    # For adjusting the Margins in inches
    left = top = width = height = Inches(1)

    # creating textBox
    txBox = slide.shapes.add_textbox(left, top,
                                    width, height)

    # creating textFrames
    tf = txBox.text_frame
    tf.text = f"Texto leido de JIRA ({issue['key']}):\n{issue['fields']['summary']}\n\n"

    # adding Paragraphs
    tf.add_paragraph()

    return ppt

def upload_to_s3(file_path, file_name):
    session = boto3.Session(
        aws_access_key_id=os.environ['AWS_S3_USERNAME'],
        aws_secret_access_key=os.environ['AWS_S3_PASSWORD'] 
    )

    # Upload the file
    s3_client = session.client('s3')
    response = s3_client.upload_file(
        file_path, 
        os.environ['AWS_S3_BUCKET'], 
        f'PoC-ppt/{file_name}', 
        ExtraArgs={'ACL': "public-read", 'ContentType': "application/vnd.openxmlformats-officedocument.presentationml.presentation"})
    return response

# Testing locally
if __name__ == '__main__':
    event = {}
    event["queryStringParameters"] = {}
    event["queryStringParameters"]["issueKey"] = "BSDROAD-50"
    print(lambda_handler(event,{}))