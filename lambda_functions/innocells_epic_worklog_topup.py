"""
Function to topup worklog values to epic fields.
"""
import json
from lightlibrary.atlassian_jira import issue_get
from lightlibrary.atlassian_jira import issue_update
from lightlibrary.atlassian_jira import jql_query

# pylint: disable=unused-argument
def lambda_handler(event, context):
    """ Main function """
    # Management variables
    issue = json.loads(event['body'])['issue']

    # Dictionary to store new values
    fields = {}

    # Only process issues with parent.
    if 'parent' in issue['fields']:
        epic = issue_get(issue['fields']['parent']['self'])

        server = epic['self'].split("/")[2]
        jql = f"parentEpic={epic['key']} AND issueType=Timetrackings&fields=timetracking"

        # Time spent agregado = E timetracking.timeSpentSeconds
        fields['customfield_10359'] = 0
        # Time remaining agregado = E timetracking.remainingEstimateSeconds
        fields['customfield_10360'] = 0
        # Time estimate agregado = E timetracking.originalEstimateSeconds
        fields['customfield_10361'] = 0

        # Search in descendats.
        for timetracking in jql_query(server, jql)['issues']:
            if 'timetracking' in timetracking['fields']:
                tt_fields = timetracking['fields']['timetracking']
                fields['customfield_10359'] += tt_fields['timeSpentSeconds'] if 'timeSpentSeconds' in tt_fields else 0
                fields['customfield_10360'] += tt_fields['remainingEstimateSeconds'] if 'remainingEstimateSeconds' in tt_fields else 0
                fields['customfield_10361'] += tt_fields['originalEstimateSeconds'] if 'originalEstimateSeconds' in tt_fields else 0

        # Convert to hours and round it.
        fields['customfield_10359'] = round(fields['customfield_10359'] / 3600)
        fields['customfield_10360'] = round(fields['customfield_10360'] / 3600)
        fields['customfield_10361'] = round(fields['customfield_10361'] / 3600)

        # Remove matching values from update
        # pylint: disable=line-too-long
        if 'customfield_10359' in epic['fields'] and epic['fields']['customfield_10359'] is not None and round(epic['fields']['customfield_10359']) == fields['customfield_10359']:
            del fields['customfield_10359']
        if 'customfield_10360' in epic['fields'] and epic['fields']['customfield_10359'] is not None and round(epic['fields']['customfield_10360']) == fields['customfield_10360']:
            del fields['customfield_10360']
        if 'customfield_10361' in epic['fields'] and epic['fields']['customfield_10359'] is not None and round(epic['fields']['customfield_10361']) == fields['customfield_10361']:
            del fields['customfield_10361']

        # Update different fields and prepare exit messages
        if len(fields) > 0:
            issue_update(epic['self'], fields)
            message = f"{issue['key']}@{epic['key']} updated!"
            response = {'body': message, 'statusCode': 200}
        else:
            message = f"{issue['key']}@{epic['key']} untouched!"
            response = {'body': message, 'statusCode': 204}
    else:
        message = f"{issue['key']} is orphan!"
        response = {'body': message, 'statusCode': 204}

    print (message)
    return response

# Testing locally
def main_debug():
    """ Debug direct entry point """
    dummy_payload = {}
    dummy_server = "innocells-uat.atlassian.net"
    dummy_list = "PMOEMPR-1228"

    for dummy_issue in dummy_list.split(","):
        issue = {}
        issue['issue'] = issue_get(f'https://{dummy_server}/rest/api/2/issue/{dummy_issue}')
        dummy_payload['body'] = json.dumps(issue)
        print(lambda_handler(dummy_payload ,""))

if __name__ == '__main__':
    main_debug()
