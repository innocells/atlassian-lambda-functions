import os, logging
from flask import Flask, jsonify, request
from jira import JIRA

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

app = Flask(__name__)
@app.route("/issue_update", methods = ['POST'])

def issue_update():
    # transform a dict into an application/json response 
    issue = request.get_json( )
    parent = issue['fields']['customfield_10400']

    headers = JIRA.DEFAULT_OPTIONS["headers"].copy()
    personal_access_token = os.environ['JIRA_PERSONAL_ACCESS_TOKEN']

    headers["Authorization"] = f"Basic {personal_access_token}"

    jira=JIRA(server="https://dev-innocells.atlassian.net", options={"headers": headers})

    parent_issue = jira.issue(parent, fields="summary")
    parent_issue.update(fields={'summary': issue['fields']['summary']})

    return jsonify({"message": f"It Works! {issue['key']}<->{parent}"})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=47612, debug=False)