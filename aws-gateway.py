import logging, json
from flask import Flask, jsonify, request

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

# Publish endpoints.
# Emulate AWS Lambda gateway call to different functions.
app = Flask(__name__)

@app.route("/connection_test", methods = ['POST'])
def connection_test():
    """ """
    from lambda_functions.connection_test import lambda_handler
    event = json.loads("{}")
    event['body'] = json.dumps(request.get_json())
    response = lambda_handler(event, json.loads("{}"))
    return response['body'], response['statusCode']

@app.route("/update_master_hdev", methods = ['POST'])
def update_master_hdev():
    """ """
    from lambda_functions.update_master_hdev import lambda_handler
    event = json.loads("{}")
    event['body'] = json.dumps(request.get_json())
    response =  lambda_handler(event, json.loads("{}"))
    return response['body'], response['statusCode']

@app.route("/excel", methods = ['GET'])
def excel():
    """ """
    from lambda_functions.excel import lambda_handler
    event = json.loads("{}")
    event['queryStringParameters'] = request.args.to_dict()
    response =  lambda_handler(event, json.loads("{}"))
    return response['body'], response['statusCode']

@app.route("/kongo_ppt", methods = ['GET'])
def kongo_ppt():
    """ """
    from lambda_functions.kongo_ppt import lambda_handler
    event = json.loads("{}")
    event['queryStringParameters'] = request.args.to_dict()
    response =  lambda_handler(event, json.loads("{}"))
    return response['body'], response['statusCode']

@app.route("/epic_worklog_topup", methods = ['POST'])
def epic_worklog_topup():
    """ """
    from lambda_functions.innocells_epic_worklog_topup import lambda_handler
    event = json.loads("{}")
    event['body'] = json.dumps(request.get_json())
    response =  lambda_handler(event, json.loads("{}"))
    return response['body'], response['statusCode']

# Daemon controller.
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=47612, debug=False)
